---
- name: "Create server '{{ server.name }}' directory"
  file:
    state: directory
    path: "{{ spigot_servers_dir }}/{{ server.name }}"
    mode: '0770'

- name: "Create '{{ server.name }}' plugins directory"
  file:
    state: directory
    path: "{{ spigot_servers_dir }}/{{ server.name }}/plugins"
    mode: '0770'

- name: "Set server '{{ server.name }}' world name"
  set_fact:
    world: "{{ server.world | default('world') }}"

- name: "Template server '{{ server.name }}' configuration and lists"
  include_tasks: template_configs.yaml

- name: "Set volumes"
  set_fact:
    spigot_volumes: |
      [
        "{{ spigot_servers_dir }}/{{ server.name }}/plugins:/plugins",
        "{{ spigot_servers_dir }}/{{ server.name }}/server.properties:/server/server.properties",
        "{{ spigot_servers_dir }}/{{ server.name }}/ops.json:/server/ops.json",
        "{{ spigot_servers_dir }}/{{ server.name }}/whitelist.json:/server/whitelist.json",
        "{{ spigot_servers_dir }}/{{ server.name }}/banned-players.json:/server/banned-players.json",
        {% if server.icon is defined %}
        "{{ spigot_servers_dir }}/{{ server.name }}/server-icon.png:/server/server-icon.png",
        {% endif %}
        "{{ spigot_servers_dir }}/{{ server.name }}/{{ world }}:/server/{{ world }}",
        "{{ spigot_servers_dir }}/{{ server.name }}/{{ world }}_nether:/server/{{ world }}_nether",
        "{{ spigot_servers_dir }}/{{ server.name }}/{{ world }}_the_end:/server/{{ world }}_the_end",
      ]

- name: "Download spigot plugins for server '{{ server.name }}'"
  get_url:
    dest: "{{ spigot_servers_dir }}/{{ server.name }}/plugins"
    url: "{{ item.url }}"
    mode: '0664'
  loop: "{{ server.plugins }}"
  when: server.plugins is defined
  register: server_plugins

- name: "Deploy server '{{ server.name }}'"
  docker_container:
    name: "mc_{{ server.name }}"
    image: eyegog/spigot_server:latest
    pull: true
    ports:
      - "{{ server.port | default(25565) }}:25565"
    volumes: "{{ spigot_volumes }}"
    restart_policy: unless-stopped
  register: deployed

- name: "Restart spigot server '{{ server.name }}'"
  docker_container:
    name: "mc_{{ server.name }}"
    restart: true
  when: (not deployed.changed) and (server_icon.changed or server_plugins.changed)
